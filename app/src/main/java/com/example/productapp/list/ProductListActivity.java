package com.example.productapp.list;

import static com.example.productapp.db.ProductContract.ProductEntry.CONTENT_URI;
import static com.example.productapp.db.ProductContract.ProductEntry.PRODUCT_CHECK;
import static com.example.productapp.db.ProductContract.ProductEntry.PRODUCT_NAME;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.support.v7.widget.helper.ItemTouchHelper.SimpleCallback;
import android.widget.Button;
import com.example.productapp.R;
import com.example.productapp.add.AddProductActivity;
import com.example.productapp.db.Product;
import com.example.productapp.list.ProductAdapter.onCheckBoxClickListener;
import com.example.productapp.list.ProductAdapter.onClickListener;

public class ProductListActivity extends AppCompatActivity implements onClickListener,
    onCheckBoxClickListener {

  private ProductAdapter productAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_product_list);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    Button addProductButton = findViewById(R.id.add_product);
    addProductButton.setOnClickListener(view -> {
      Intent intent = new Intent(this, AddProductActivity.class);
      startActivity(intent);
    });

    RecyclerView recyclerView = findViewById(R.id.recycler_view);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    productAdapter = new ProductAdapter(this);
    productAdapter.setClickListener(this);
    productAdapter.setCheckBoxClick(this);
    recyclerView.setAdapter(productAdapter);

    ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);
    itemTouchHelper.attachToRecyclerView(recyclerView);
  }

  private ItemTouchHelper.SimpleCallback itemTouchCallback = new SimpleCallback(0,
      ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView,
        @NonNull RecyclerView.ViewHolder viewHolder,
        @NonNull RecyclerView.ViewHolder target) {
      return false;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int swipeDir) {

      int position = viewHolder.getAdapterPosition();
      getContentResolver().delete(CONTENT_URI, PRODUCT_NAME + " = ? ",
          new String[]{productAdapter.getProducts().get(position).getName()});

      productAdapter.getProducts().remove(position);
      productAdapter.notifyDataSetChanged();
    }
  };

  @Override
  protected void onResume() {
    super.onResume();
    setProducts();
  }

  private void setProducts() {

    productAdapter.clearData();
    Cursor cursor = null;

    try {
      cursor = getContentResolver().query(CONTENT_URI, null, null, null, null);
      if (cursor.moveToFirst()) {
        do {
          Product product = new Product(cursor.getString(1),
              cursor.getDouble(2), cursor.getInt(3),
              cursor.getInt(4));
          productAdapter.swapData(product);
        } while (cursor.moveToNext());
      }

    } finally {
      if (cursor != null) {
        cursor.close();
      }
    }
  }

  @Override
  public void onItemClick(Product product) {
    Intent intent = new Intent(this, AddProductActivity.class);
    intent.putExtra("Product", product);
    startActivity(intent);
  }

  @Override
  public void onCheckBoxClick(boolean isChecked, String name) {

    ContentValues values = new ContentValues();
    values.clear();

    if (isChecked) {
      values.put(PRODUCT_CHECK, 1);
      getContentResolver()
          .update(CONTENT_URI, values, PRODUCT_NAME + " = ? ", new String[]{name});
    } else {
      values.put(PRODUCT_CHECK, 0);
      getContentResolver()
          .update(CONTENT_URI, values, PRODUCT_NAME + " = ? ", new String[]{name});
    }
  }
}
