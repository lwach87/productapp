package com.example.productapp.list;

import static com.example.productapp.MainActivity.MODE_PRIVATE;
import static com.example.productapp.MainActivity.PREFS_NAME;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import com.example.productapp.MainActivity;
import com.example.productapp.R;
import com.example.productapp.db.Product;
import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

  private List<Product> products = new ArrayList<>();
  private onClickListener clickListener;

  private onCheckBoxClickListener checkBoxClick;
  private Context context;

  public interface onClickListener {

    void onItemClick(Product product);
  }

  public interface onCheckBoxClickListener {

    void onCheckBoxClick(boolean isChecked, String productName);
  }

  public void setClickListener(onClickListener clickListener) {
    this.clickListener = clickListener;
  }

  public void setCheckBoxClick(onCheckBoxClickListener checkBoxClick) {
    this.checkBoxClick = checkBoxClick;
  }

  public ProductAdapter(Context context) {
    this.context = context;
  }

  @NonNull
  @Override
  public ProductAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    View view = LayoutInflater.from(viewGroup.getContext())
        .inflate(R.layout.single_product_item, viewGroup, false);
    return new ProductAdapter.ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ProductAdapter.ViewHolder viewHolder, int position) {
    viewHolder.nameTextView.setText(products.get(position).getName());
    viewHolder.priceTextView.setText(String.valueOf(products.get(position).getPrice()));
    viewHolder.quantityTextView.setText(String.valueOf(products.get(position).getQuantity()));
    viewHolder.checkBox.setChecked(products.get(position).getProductCheck() == 1);

    getSharedPreferences(viewHolder);
  }

  @Override
  public int getItemCount() {
    return products.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {

    private TextView nameTextView, priceTextView, quantityTextView, currencyTextView;
    private CheckBox checkBox;

    public ViewHolder(@NonNull View itemView) {
      super(itemView);

      nameTextView = itemView.findViewById(R.id.tv_name);
      priceTextView = itemView.findViewById(R.id.tv_price);
      quantityTextView = itemView.findViewById(R.id.tv_quantity);
      currencyTextView = itemView.findViewById(R.id.tv_currency);
      checkBox = itemView.findViewById(R.id.checkBox);

      itemView.setOnClickListener(view -> {
        int position = getAdapterPosition();
        clickListener.onItemClick(products.get(position));
      });

      checkBox.setOnClickListener(view -> {
        int position = getAdapterPosition();
        checkBoxClick.onCheckBoxClick(checkBox.isChecked(), products.get(position).getName());
      });
    }

  }

  public List<Product> getProducts() {
    return products;
  }

  public void swapData(Product product) {
    products.add(product);
    notifyDataSetChanged();
  }

  public void clearData() {
    products.clear();
  }

  private void getSharedPreferences(@NonNull ViewHolder viewHolder) {
    SharedPreferences preferences = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
    int fontSize = preferences.getInt(MainActivity.FONT_SIZE, 0);
    String color = preferences.getString(MainActivity.FONT_COLOR, "");

    setTextViewStyle(viewHolder.nameTextView, fontSize, parseColor(color));
    setTextViewStyle(viewHolder.priceTextView, fontSize, parseColor(color));
    setTextViewStyle(viewHolder.currencyTextView, fontSize, parseColor(color));
    setTextViewStyle(viewHolder.quantityTextView, fontSize, parseColor(color));
  }

  private void setTextViewStyle(TextView textView, int fontSize, int color) {
    textView.setTextSize(fontSize);
    textView.setTextColor(color);
  }

  private int parseColor(String color) {
    switch (color) {
      case "Red":
        return Color.RED;
      case "Green":
        return Color.GREEN;
      case "Blue":
        return Color.BLUE;
      case "Black":
        return Color.BLACK;
      default:
        return Color.BLACK;
    }
  }
}
