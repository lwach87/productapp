package com.example.productapp.pref;

import static com.example.productapp.MainActivity.FONT_COLOR;
import static com.example.productapp.MainActivity.FONT_SIZE;
import static com.example.productapp.MainActivity.PREFS_NAME;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import com.example.productapp.R;

public class PrefActivity extends AppCompatActivity {

  private EditText fontSizeEditText;
  private Spinner colorSpinner;
  private ArrayAdapter<String> adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_pref);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    fontSizeEditText = findViewById(R.id.ed_font);
    colorSpinner = findViewById(R.id.spinner);

    adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
        getResources().getStringArray(R.array.colors));
    colorSpinner.setAdapter(adapter);

    Button saveButton = findViewById(R.id.button_save);
    saveButton.setOnClickListener(view -> savePreferences());

    loadPreferences();
  }

  private void savePreferences() {
    SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();

    String fontSize = fontSizeEditText.getText().toString();

    if (!fontSize.isEmpty() && Integer.parseInt(fontSize) > 0) {
      editor.putInt(FONT_SIZE, Integer.parseInt(fontSize));
      editor.putString(FONT_COLOR, colorSpinner.getSelectedItem().toString());
      editor.apply();

      finish();
    } else {
      Toast.makeText(getApplicationContext(), "Font size can't be <= 0 or empty!",
          Toast.LENGTH_SHORT).show();
    }
  }

  private void loadPreferences() {
    SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

    int fontSize = preferences.getInt(FONT_SIZE, 0);
    String color = preferences.getString(FONT_COLOR, "");

    fontSizeEditText.setText(String.valueOf(fontSize));

    int selectedColor = adapter.getPosition(color);
    colorSpinner.setSelection(selectedColor);
  }
}
