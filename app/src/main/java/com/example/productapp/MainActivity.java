package com.example.productapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import com.example.productapp.list.ProductListActivity;
import com.example.productapp.pref.PrefActivity;

public class MainActivity extends AppCompatActivity {

  public static final String FONT_SIZE = "view.font_size";
  public static final String FONT_COLOR = "view.font_color";
  public static final String PREFS_NAME = "view.prefs";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Button optionsButton = findViewById(R.id.options_button);
    optionsButton.setOnClickListener(view -> {
      Intent intent = new Intent(this, PrefActivity.class);
      startActivity(intent);
    });

    Button listButton = findViewById(R.id.list_button);
    listButton.setOnClickListener(view -> {
      Intent intent = new Intent(this, ProductListActivity.class);
      startActivity(intent);
    });
  }
}
