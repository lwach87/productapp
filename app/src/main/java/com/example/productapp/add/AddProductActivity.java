package com.example.productapp.add;

import static com.example.productapp.db.ProductContract.ProductEntry.CONTENT_URI;
import static com.example.productapp.db.ProductContract.ProductEntry.PRODUCT_NAME;
import static com.example.productapp.db.ProductContract.ProductEntry.PRODUCT_PRICE;
import static com.example.productapp.db.ProductContract.ProductEntry.PRODUCT_QUANTITY;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.productapp.R;
import com.example.productapp.db.Product;

public class AddProductActivity extends AppCompatActivity implements OnClickListener {

  private EditText editTextName;
  private EditText editTextPrice;
  private EditText editTextQuantity;
  private Product product;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_product);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    editTextName = findViewById(R.id.et_name);
    editTextPrice = findViewById(R.id.et_price);
    editTextQuantity = findViewById(R.id.et_quantity);

    Button addProductButton = findViewById(R.id.activity_add_button);
    addProductButton.setOnClickListener(this);

    Intent intent = getIntent();
    if (intent != null && intent.hasExtra("Product")) {

      product = intent.getExtras().getParcelable("Product");
      addProductButton.setText(getString(R.string.update_product));

      editTextName.setText(product.getName());
      editTextPrice.setText(String.valueOf(product.getPrice()));
      editTextQuantity.setText(String.valueOf(product.getQuantity()));
    }
  }

  @Override
  public void onClick(View view) {

    ContentValues values = new ContentValues();
    values.clear();

    String name = editTextName.getText().toString();
    String price = editTextPrice.getText().toString();
    String quantity = editTextQuantity.getText().toString();

    if (!name.isEmpty() && !price.isEmpty() && !quantity.isEmpty()) {

      values.put(PRODUCT_NAME, editTextName.getText().toString());
      values.put(PRODUCT_PRICE, editTextPrice.getText().toString());
      values.put(PRODUCT_QUANTITY, editTextQuantity.getText().toString());

      if (product == null) {
        getContentResolver().insert(CONTENT_URI, values);
      } else {
        getContentResolver()
            .update(CONTENT_URI, values, PRODUCT_NAME + " = ? ", new String[]{product.getName()});
      }

      finish();

    } else {
      Toast.makeText(this, "No empty values enabled!", Toast.LENGTH_SHORT)
          .show();
    }
  }
}
