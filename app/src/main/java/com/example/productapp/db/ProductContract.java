package com.example.productapp.db;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class ProductContract {

  public static final String AUTHORITY = "com.example.productapp";
  public static final Uri BASE_URI = Uri.parse("content://" + AUTHORITY);

  public static final class ProductEntry implements BaseColumns {

    public static final String TABLE_NAME = "products";
    public static final String PRODUCT_NAME = "name";
    public static final String PRODUCT_PRICE = "price";
    public static final String PRODUCT_QUANTITY = "quantity";
    public static final String PRODUCT_CHECK = "product_check";

    public static final Uri CONTENT_URI = BASE_URI.buildUpon().appendPath(TABLE_NAME).build();
    public static final String SORT_ORDER = PRODUCT_NAME + " ASC";

    public static Uri buildProductUri(long id) {
      return ContentUris.withAppendedId(CONTENT_URI, id);
    }
  }
}
