package com.example.productapp.db;

import static com.example.productapp.db.ProductContract.ProductEntry.PRODUCT_CHECK;
import static com.example.productapp.db.ProductContract.ProductEntry.PRODUCT_NAME;
import static com.example.productapp.db.ProductContract.ProductEntry.PRODUCT_PRICE;
import static com.example.productapp.db.ProductContract.ProductEntry.PRODUCT_QUANTITY;
import static com.example.productapp.db.ProductContract.ProductEntry.TABLE_NAME;
import static com.example.productapp.db.ProductContract.ProductEntry._ID;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ProductDatabase extends SQLiteOpenHelper {

  private static final int DATABASE_VERSION = 1;
  private static final String DATABASE_NAME = "product.db";

  public ProductDatabase(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {

    final String SQL_CREATE_PRODUCT_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
        _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
        PRODUCT_NAME + " TEXT NOT NULL," +
        PRODUCT_PRICE + " DOUBLE NOT NULL," +
        PRODUCT_QUANTITY + " INTEGER NOT NULL," +
        PRODUCT_CHECK + " INTEGER DEFAULT 0" +
        " );";

    db.execSQL(SQL_CREATE_PRODUCT_TABLE);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("ALTER TABLE " + TABLE_NAME);
    onCreate(db);
  }
}
