package com.example.productapp.db;

import static com.example.productapp.db.ProductContract.AUTHORITY;
import static com.example.productapp.db.ProductContract.ProductEntry.PRODUCT_NAME;
import static com.example.productapp.db.ProductContract.ProductEntry.SORT_ORDER;
import static com.example.productapp.db.ProductContract.ProductEntry.TABLE_NAME;
import static com.example.productapp.db.ProductContract.ProductEntry.buildProductUri;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;


public class ProductProvider extends ContentProvider {

  private static final UriMatcher sUriMatcher = buildUriMatcher();
  private ProductDatabase mDbHelper;

  private static final int PRODUCT = 100;
  private static final int PRODUCT_ID = 101;

  private static UriMatcher buildUriMatcher() {

    final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

    matcher.addURI(AUTHORITY, TABLE_NAME, PRODUCT);
    matcher.addURI(AUTHORITY, TABLE_NAME + "/#", PRODUCT_ID);

    return matcher;
  }

  @Override
  public boolean onCreate() {
    mDbHelper = new ProductDatabase(getContext());
    return true;
  }

  @Override
  public String getType(@NonNull Uri uri) {

    switch (sUriMatcher.match(uri)) {
      case PRODUCT:
        return "vnd.android.cursor.dir/" + AUTHORITY + "/" + TABLE_NAME;
      case PRODUCT_ID:
        return "vnd.android.cursor.item/" + AUTHORITY + "/" + TABLE_NAME;
      default:
        return null;
    }
  }

  @Override
  public Cursor query(@NonNull Uri uri, String[] projection, String selection,
      String[] selectionArgs, String sortOrder) {

    Cursor cursor;
    SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

    switch (sUriMatcher.match(uri)) {

      case PRODUCT: {
        builder.setTables(TABLE_NAME);
        if (TextUtils.isEmpty(sortOrder)) {
          sortOrder = SORT_ORDER;
        }
        break;
      }

      case PRODUCT_ID: {
        builder.setTables(TABLE_NAME);
        builder.appendWhere(PRODUCT_NAME + " = " + uri.getLastPathSegment());
        break;
      }

      default:
        throw new UnsupportedOperationException("Unknown uri: " + uri);
    }

    cursor = builder.query(
        mDbHelper.getReadableDatabase(),
        projection,
        selection,
        selectionArgs,
        null,
        null,
        sortOrder);

    cursor.setNotificationUri(getContext().getContentResolver(), uri);
    return cursor;
  }

  @Override
  public Uri insert(@NonNull Uri uri, ContentValues values) {

    Uri returnUri;

    switch (sUriMatcher.match(uri)) {
      case PRODUCT: {
        long _id = mDbHelper.getWritableDatabase().insert(TABLE_NAME, null, values);
        if (_id > 0) {
          returnUri = buildProductUri(_id);
        } else {
          throw new android.database.SQLException("Failed when inserting the row into" + uri);
        }
        break;
      }
      default:
        throw new UnsupportedOperationException("Unknown uri: " + uri);
    }
    getContext().getContentResolver().notifyChange(uri, null);
    return returnUri;
  }

  @Override
  public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {

    int rowsDeleted;

    switch (sUriMatcher.match(uri)) {
      case PRODUCT:
        rowsDeleted = mDbHelper.getWritableDatabase().delete(TABLE_NAME, selection, selectionArgs);
        break;

      default:
        throw new UnsupportedOperationException("Unknown uri: " + uri);
    }

    if (selection == null || rowsDeleted != 0) {
      getContext().getContentResolver().notifyChange(uri, null);
    }
    return rowsDeleted;
  }

  @Override
  public int update(@NonNull Uri uri, ContentValues values, String selection,
      String[] selectionArgs) {

    int rowsUpdated;

    switch (sUriMatcher.match(uri)) {
      case PRODUCT:
        rowsUpdated = mDbHelper.getWritableDatabase()
            .update(TABLE_NAME, values, selection, selectionArgs);
        break;

      default:
        throw new UnsupportedOperationException("Unknown uri: " + uri);
    }
    if (rowsUpdated != 0) {
      getContext().getContentResolver().notifyChange(uri, null);
    }
    return rowsUpdated;
  }
}
