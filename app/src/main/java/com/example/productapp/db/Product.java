package com.example.productapp.db;

import android.os.Parcel;
import android.os.Parcelable;

public class Product implements Parcelable {

  private String name;
  private double price;
  private int quantity;
  private int productCheck;

  public Product(String name, double price, int quantity, int productCheck) {
    this.name = name;
    this.price = price;
    this.quantity = quantity;
    this.productCheck = productCheck;
  }

  protected Product(Parcel in) {
    name = in.readString();
    price = in.readDouble();
    quantity = in.readInt();
    productCheck = in.readInt();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  public int getProductCheck() {
    return productCheck;
  }

  public void setProductCheck(int productCheck) {
    this.productCheck = productCheck;
  }

  public static final Creator<Product> CREATOR = new Creator<Product>() {
    @Override
    public Product createFromParcel(Parcel in) {
      return new Product(in);
    }

    @Override
    public Product[] newArray(int size) {
      return new Product[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel parcel, int i) {
    parcel.writeString(name);
    parcel.writeDouble(price);
    parcel.writeInt(quantity);
    parcel.writeInt(productCheck);
  }
}
